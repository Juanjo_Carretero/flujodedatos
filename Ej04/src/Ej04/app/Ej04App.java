package Ej04.app;

import javax.swing.JOptionPane;

public class Ej04App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARACI�N DE VARIABLES
		String radio = JOptionPane.showInputDialog("Introduce el radio"); //ENTRAMOS POR TECLADO EL NOMBRE QUE QUERAMOS
		
		//CONVERSI�N DE STRING A DOUBLE
		double radiodouble = Double.parseDouble(radio);
		
		//CALCULO EL AREA
		double area = 3.1416 * Math.pow(radiodouble,2);
		
		//MUESTRO POR PANTALLA UN MENSAJE DE DIALOGO CON EL RESULTADO
		JOptionPane.showMessageDialog(null, "El area de tu ciruclo �s: " + area);
	}

}
