package Ej06.app;

import javax.swing.JOptionPane;

public class Ej06App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DEFINICION DE VARIABLES
		String numero = JOptionPane.showInputDialog("Introduce un numero"); //ENTRAMOS POR TECLADO EL NUMERO QUE QUERAMOS APLICAR EL IVA

		//CONVERSI�N DE VARIABLES
		double numerodouble = Double.parseDouble(numero);
		
		//IVA
		final double IVA = 21;
		
		//TOTAL IVA
		double totaliva = (numerodouble * 21)/100;
		
		//TOTAL PRECIO CON EL IVA APLICADO
		double total = numerodouble + totaliva;
				
		//MOSTRAMOS POR PANTALLA
		JOptionPane.showMessageDialog(null,"El total con el IVA incluido �s: " + total);
		
		
		
		
	}

}
