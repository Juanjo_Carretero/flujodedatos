package Ej10.app;

import javax.swing.JOptionPane;

public class Ej10App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DEFINICION DE VARIABLES
		String numeroventas = JOptionPane.showInputDialog("Introduce un numero de ventas"); //ENTRAMOS POR TECLADO EL NUMERO DE VENTAS QUE QUERAMOS
		double totalventas=0;
		
		//CONVERSIÓN DE VARIABLES
		int numeroint = Integer.parseInt(numeroventas);
		
		for (int i = 1; i <= numeroint; i++) {
			
			String ventas = JOptionPane.showInputDialog("Cual ha sido el total de la venta "+i);
			
			double ventasdouble = Double.parseDouble(ventas);
			
			totalventas = totalventas + ventasdouble;
		}
		
		//MOSTRAMOS FINALMENTE POR PANTALLA EL TOTAL DE LAS VENTAS
		JOptionPane.showMessageDialog(null,"Total de ventas: "+totalventas+"€");
				
	}

}
