package Ej12.app;

import javax.swing.JOptionPane;

public class Ej12App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARACIÓN DE VARIABLES
		String contraseña = JOptionPane.showInputDialog("Introduce una contraseña"); //ENTRAMOS POR TECLADO LA CONTRASEÑA QUE QUERAMOS
		String check;
		int intentos = 1;
		boolean correcta = false;
		
		//DO WHILE PARA IR PREGUNTANDO
		do {
			//GUARDO EN LA VARIABLE CHECK EL INTENTO DE CONTRASEÑA PARA SEGUIDAMENTE COMPROBAR SI COINCIDE1
			check = JOptionPane.showInputDialog("Repita la contraseña");
			
			//SI CONTRASEÑA ES IGUAL A CHECK ENHORABONA
			if(contraseña.equals(check)) {
				JOptionPane.showMessageDialog(null,"Enhorabona");
				correcta = true;
				intentos = 3;
			}else {
				intentos++;
			}
			

		} while (intentos <= 3 || correcta == true);
		
	}

}
