package Ej13.app;

import javax.swing.JOptionPane;

public class Ej13App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARACI�N DE VARIABLES
		String n1 = JOptionPane.showInputDialog("Introduce el primer n�mero"); //ENTRAMOS POR TECLADO EL N1 QUE QUERAMOS
		
		String n2 = JOptionPane.showInputDialog("Introduce el segundo n�mero"); //ENTRAMOS POR TECLADO EL N2 QUE QUERAMOS
		
		String operador = JOptionPane.showInputDialog("Introduce el operador. (+, -, *, /, ^, %)"); //ENTRAMOS POR TECLADO EL OPERADOR QUE QUERAMOS
		
		//CONVERSI�N DE VARIABLES
		int n1int = Integer.parseInt(n1);
		
		int n2int = Integer.parseInt(n2);
		
		//CONDICIONAL PARA HACER UNA OPERACI�N O OTRA DEPENDIENDO DE EL OPERADOR SELECCIONADO
		if (operador.equals("+")) {
			JOptionPane.showMessageDialog(null,"La suma de "+n1+" + "+n2+" = "+(n1int+n2int));
		}else if(operador.equals("-")){
			JOptionPane.showMessageDialog(null,"La resta de "+n1+" - "+n2+" = "+(n1int-n2int));
		}else if(operador.equals("*")){
			JOptionPane.showMessageDialog(null,"La multiplicaci�n de "+n1+" * "+n2+" = "+(n1int*n2int));
		}else if(operador.equals("/")){
			JOptionPane.showMessageDialog(null,"La divisi�n de "+n1+" / "+n2+" = "+(n1int/n2int));
		}else if(operador.equals("^")){
			JOptionPane.showMessageDialog(null,n1+" ^ "+n2+" = "+(Math.pow(n1int, n2int)));
		}else if(operador.equals("%")){
			JOptionPane.showMessageDialog(null,"El m�dulo de "+n1+" % "+n2+" = "+(n1int%n2int));
		}
	}

}
